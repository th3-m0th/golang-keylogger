# go-keylogger

This is a fork of [go-keylogger](https://github.com/kindlyfire/go-keylogger) by kindlyfire.
The difference between this fork and the original proyect is that it can now regognize stuff like return carriages, tabs and if keys were pressed while pressing CTRL or ALT keys.

This proyect exists because of [gologger](https://gitlab.com/th3-m0th/gologger) a keylogger I'm building thanks to this library and to kindlyfire's work because they did all of the hard work.

TODO:
- [X] Return carriages
- [ ] Tabs
- [ ] CTRL
- [ ] ALT
